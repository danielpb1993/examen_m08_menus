package com.example.examendanielpenarroya2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder2> {
    private ArrayList<Integer> guideImages;
    private Context mContext;

    public ArrayList<Integer> getGuideImages() {
        return guideImages;
    }

    public void setGuideImages(ArrayList<Integer> guideImages) {
        this.guideImages = guideImages;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public MyAdapter2(ArrayList<Integer> guideImages, Context mContext) {
        this.guideImages = guideImages;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.image_row, parent, false);
        return new MyAdapter2.MyViewHolder2(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder2 holder, int position) {
        holder.imageViewRecycler.setImageResource(guideImages.get(position));
    }

    @Override
    public int getItemCount() {
        return guideImages.size();
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder {
        private ImageView imageViewRecycler;
        ConstraintLayout imageRow;

        public MyViewHolder2(@NonNull View itemView) {
            super(itemView);
            imageViewRecycler = itemView.findViewById(R.id.imageViewRecycler);
            imageRow = itemView.findViewById(R.id.imageRow);
        }
    }
}
