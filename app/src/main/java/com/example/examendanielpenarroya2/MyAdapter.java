package com.example.examendanielpenarroya2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<Guide> mGuides;
    private Context mContext;

    public MyAdapter(ArrayList<Guide> mGuides, Context mContext) {
        this.mGuides = mGuides;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.guide_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.imageViewGuide.setImageResource(mGuides.get(position).getGuideImage());
        holder.textGuideName.setText(mGuides.get(position).getGuideName());
        holder.textCity.setText(mGuides.get(position).getGuideCity());
        holder.textPrice.setText(mGuides.get(position).getGuidePrice());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailsFragment detailsFragment = DetailsFragment.newInstance(
                        mGuides.get(holder.getAdapterPosition()).getGuideName(),
                        mGuides.get(holder.getAdapterPosition()).getGuideCity(),
                        mGuides.get(holder.getAdapterPosition()).getGuidePrice(),
                        mGuides.get(holder.getAdapterPosition()).getGuideDesc(),
                        String.valueOf(mGuides.get(holder.getAdapterPosition()).getGuideImage()),
                            mGuides.get(holder.getAdapterPosition()).getGuideImages()
                );
                FragmentManager fragmentManager = ((FragmentActivity)mContext).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, detailsFragment);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGuides.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageViewGuide;
        private TextView textGuideName;
        private TextView textCity;
        private TextView textPrice;
        ConstraintLayout guideRow;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewGuide = itemView.findViewById(R.id.imageViewGuide);
            textGuideName = itemView.findViewById(R.id.textGuideName);
            textCity = itemView.findViewById(R.id.textCity);
            textPrice = itemView.findViewById(R.id.textPrice);
        }
    }
}
